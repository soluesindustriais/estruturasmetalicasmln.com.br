<?
$h1         = 'Estruturas Metálicas';
$title      = 'Início';
$desc       = 'Se procura por '.$h1.', você encontra nos resultados do Soluções Industriais, receba diversos orçamentos com mais de 100 empresas do Brasil ao mesmo tempo';
$var        = 'Home';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
	<div class="title-main"> <h1>Estruturas Metálicas MLN</h1> </div>
	<ul class="cd-hero-slider autoplay">
		<li class="selected">
			<div class="cd-full-width">
				<h2>Montagem de Estrutura Metálica</h2>
				<p>Uma empresa de montagem de estrutura metálica é especializada em projetar, fabricar e instalar estruturas metálicas para edifícios, pontes, torres de transmissão, galpões industriais, entre outros. </p>
				<a href="empresa-de-montagem-de-estrutura-metalica" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Cobertura de Policarbonato</h2>
				<p>A cobertura com policarbonato alveolar são distribuídas em dois tipos, cobertura fixa e cobertura retrátil manual com puxador ou motorizada com acionamento em botoeiras ou no controle remoto.</p>
				<a href="cobertura-de-policarbonato" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Lona para toldo</h2>
				<p>Os toldos de lona protegem o ambiente onde são aplicados e são um forte elemento de decoração que podem propiciar sofisticação e requinte.</p>
				<a href="lona-para-toldo" class="cd-btn">Saiba mais</a>
			</div>
		</li>
	</ul>
	<div class="cd-slider-nav">
		<nav>
			<span class="cd-marker item-1"></span>
			<ul>
				<li class="selected"><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
			</ul>
		</nav>
	</div>
</section>
<main>
	<section class="wrapper-main">
		<div class="main-center">
			<div class=" quadro-2 ">
				<h2>Estruturas Metálicas</h2>
				<div class="div-img">
					<p>A estrutura metálica é uma estrutura composta geralmente por aço, sendo muito utilizada para fabricar suportes internos e para desenvolver revestimentos exteriores. Essa ferramenta é muito utilizada em montagem de edifícios que, por sua vez, possuem uma grande variedade de utilizações.</p>
				</div>
				<div class="gerador-svg">
					<img src="imagens/img-home/estruturas-metalicas.jpg" alt="Estruturas Metálicas" title="Estruturas Metálicas">
				</div>
			</div>
			<div class=" incomplete-box">
				<ul>
					<li>As estruturas metálicas são fabricadas a partir do processo de usinagem e são muito utilizadas por conta de sua excelente resistência e durabilidade.<br />
						Entre as diversas possibilidades de aplicações das Estruturas Metálicas, encontram-se em destaque as seguintes:</li>
					<li><br /><i class="fas fa-angle-right"></i> Edifícios</li>
					<li><i class="fas fa-angle-right"></i> Coberturas</li>
					<li><i class="fas fa-angle-right"></i> Mezaninos</li>
					<li><i class="fas fa-angle-right"></i> Decorações</li>
					<li><i class="fas fa-angle-right"></i> Fachadas</li>
					<li><i class="fas fa-angle-right"></i> Escadas</li>
				</ul>
				<a href="estruturas-metalicas" class="btn-4">Saiba mais</a>
			</div>
		</div>
		<section class="wrapper-img">
			<div class="txtcenter">
				<h2>Produtos <b>Relacionados</b></h2>
			</div>
			<div class="content-icons">
				<div class="produtos-relacionados-1">
					<figure>
						<a href="estrutura-metalica-para-mezanino">
							<div class="fig-img">
								<h2>Estrutura metálica para mezanino</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-2">
					<figure class="figure2">
						<a href="cobertura-metalica">
							<div class="fig-img2">
								<h2>Cobertura Metálica</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-3">
					<figure>
						<a href="estrutura-de-metal-para-telhado">
							<div class="fig-img">
								<h2>Estrutura de metal para telhado</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
			</div>
		</section>
		<section class="wrapper-destaque">
			<div class="destaque txtcenter">
				<h2>Galeria de <b>Produtos</b></h2>
				<div class="center-block txtcenter">
					<ul class="gallery">
						<li>
							<a href="imagens/img-home/estruturas-metalicas-1.jpg" class="lightbox" title="Edifícios Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-1.jpg" title="Edifícios Metálicas" alt="Edifícios Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-2.jpg" class="lightbox" title="Coberturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-2.jpg" alt="Coberturas Metálicas" title="Coberturas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-3.jpg" class="lightbox"  title="Edifícios Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-3.jpg" alt="Edifícios Metálicas" title="Edifícios Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-4.jpg" class="lightbox" title="Edifícios Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-4.jpg" alt="Edifícios Metálicas" title="Edifícios Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-5.jpg" class="lightbox" title="Escadas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-5.jpg" alt="Escadas Metálicas"  title="Escadas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-6.jpg" class="lightbox" title="Estruturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-6.jpg" alt="Estruturas Metálicas" title="Estruturas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-7.jpg" class="lightbox" title="Coberturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-7.jpg" alt="Coberturas Metálicas" title="Coberturas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-8.jpg" class="lightbox" title="Estruturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-8.jpg" alt="Estruturas Metálicas" title="Estruturas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-9.jpg" class="lightbox" title="Coberturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-9.jpg" alt="Coberturas Metálicas" title="Coberturas Metálicas">
							</a>
						</li>
						<li>
							<a href="imagens/img-home/estruturas-metalicas-10.jpg" class="lightbox" title="Coberturas Metálicas">
								<img src="imagens/img-home/thumbs/estruturas-metalicas-10.jpg" alt="Coberturas Metálicas" title="Coberturas Metálicas">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</section>
</main>
<?
	
	include('inc/footer.php');
?>
<script src="hero/js/modernizr.js"></script>
<script src="hero/js/main.js"></script>

<script type="text/javascript" src="slick/slick.min.js"></script>
<script>
$('.products').slick({
dots: true,
infinite: true,
speed: 300,
autoplay: true,
slidesToShow: 4,
slidesToScroll: 4,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 3,
slidesToScroll: 3,
infinite: true,
dots: true
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 2,
slidesToScroll: 2
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});
</script>
</body>
</html>
