<aside>
    <h2><a href="<?= $url ?>coberturas-categoria" title="Produtos relacionados <?= $nomeSite ?>"> Coberturas<br>Produtos relacionados</a></h2>
    <nav>
        <ul> <? include('inc/coberturas/coberturas-sub-menu.php'); ?> </ul>
    </nav> <br>
    <div class="aside-launch"></div>
    <h2><a href="<?= $url ?>" title="Outras Categorias">Outras Categorias </a></h2>
    <nav>
        <ul> <? include('inc/coberturas/coberturas-sub-menu-categoria.php'); ?> </ul>
    </nav> <br>
</aside>