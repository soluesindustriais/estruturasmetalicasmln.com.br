<aside>
    <h2><a href="<?= $url ?>estruturas-metalicas-categoria" title="Produtos relacionados <?= $nomeSite ?>"> Estruturas Metálicas<br>Produtos relacionados</a></h2>
    <nav>
        <ul> <? include('inc/estruturas-metalicas/estruturas-metalicas-sub-menu.php'); ?> </ul>
    </nav> <br>
    <div class="aside-launch"></div>
    <h2><a href="<?= $url ?>" title="Outras Categorias">Outras Categorias </a></h2>
    <nav>
        <ul> <? include('inc/estruturas-metalicas/estruturas-metalicas-sub-menu-categoria.php'); ?> </ul>
    </nav> <br>
</aside>