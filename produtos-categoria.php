<?
	$h1 = "Produtos";
	$title = "Produtos";
	$desc = "Produtos sobre os produtos e serviços comercializados pela empresa. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco agora mesmo";
	$var = "Produtos";
	include('inc/head.php');
?>
	</head>
	<body>
	<? include('inc/topo.php');?>
	<div class="wrapper">
	 	<main>
	 		<div class="content">
	 			<div id="breadcrumb" itemscope itemtype="http://schema.org/breadcrumb">
	 				<a rel="home" itemprop="url" href="<?=$url?>" title="home">
	 					<span itemprop="title">
	 						<i class="fa fa-home" aria-hidden="true"></i>Home
	 					</span>
	 				</a> »
					<strong><span class="page" itemprop="title">Produtos</span></strong>
	 			</div>
				<h1>Produtos</h1>
				<article class="full">
					<p>Encontre diversos fornecedores para estruturas e coberturas, cote agora mesmo!</p>
					<ul class="thumbnails-main">
						<li>
							<a rel="nofollow" href="<?=$url?>cobertura-metalica-categoria" title="Categoria - Cobertura Metálica">
								<img src="<?$url?>imagens/cobertura-metalica.jpg" alt="Cobertura Metálica" title="Cobertura Metálica"/>
							</a>
							<h2>
								<a href="<?=$url?>cobertura-metalica-categoria" title="Categoria - Cobertura Metálica">
									Cobertura Metálica
								</a>
							</h2>
						</li>
						<li>
							<a rel="nofollow" href="<?=$url?>coberturas-categoria" title="Categoria - Coberturas">
								<img src="<?$url?>imagens/coberturas.jpg" alt="Coberturas" title="Coberturas"/>
							</a>
							<h2>
								<a href="<?=$url?>coberturas-categoria" title="Categoria - Coberturas">
									Coberturas
								</a>
							</h2>
						</li>
						<li>
							<a rel="nofollow" href="<?=$url?>estruturas-metalicas-categoria" title="Categoria - Estruturas Metálicas">
								<img src="<?$url?>imagens/estruturas-metalicas.jpg" alt="Estruturas Metálicas" title="Estruturas Metálicas"/>
							</a>
							<h2>
								<a href="<?=$url?>estruturas-metalicas-categoria" title="Categoria - Estruturas Metálicas">
									Estruturas Metálicas
								</a>
							</h2>
						</li>
						<li>
							<a rel="nofollow" href="<?=$url?>lona-para-cobertura-categoria" title="Categoria - Lona Para Cobertura">
								<img src="<?$url?>imagens/lona-para-cobertura.jpg" alt="Lona Para Cobertura" title="Lona Para Cobertura"/>
							</a>
							<h2>
								<a href="<?=$url?>lona-para-cobertura-categoria" title="Categoria - Lona Para Cobertura">
									Lona Para Cobertura
								</a>
							</h2>
						</li>
					</ul>
				</article>
	 		</div>
	 	</main>
	 </div>
	 <? include('inc/footer.php');?>
	</body>
</html>
