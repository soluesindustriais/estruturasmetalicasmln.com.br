<?php
$minisite = "mundial-lev";
$subdominio = "mundial-lev";
$linkminisite = "inc/$minisite/";


$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";

// Obtém o nome do arquivo atual sem a extensão
$nomeArquivo = pathinfo(basename(__FILE__), PATHINFO_FILENAME);

// Remove os hífens e transforma a primeira letra de cada palavra em maiúscula
$nomeVariavel = ucwords(str_replace('-', ' ', $nomeArquivo));

$h1 = $nomeVariavel;
$title = $nomeVariavel;
$desc = "Saiba como o fechamento de galpão com telha pode proteger e isolar ambientes industriais e comerciais. Descubra as vantagens das telhas metálicas e de fibrocimento.";
$key = "mpi,sample,lorem,ipsum";
$legendaImagem = "Foto ilustrativa de Armário de aço tipo roupeiro preço";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$nomeurl = pathinfo(basename($_SERVER["PHP_SELF"]), PATHINFO_FILENAME);
include ("$linkminisite"."inc/head.php");
include ("$linkminisite"."inc/fancy.php");
?>
<style>
  body {
    scroll-behavior: smooth;
  }
  <?php
  include "$linkminisite" . "css/header-script.css";
  include "$linkminisite" . "css/style.css";
  include "$linkminisite" . "css/mpi-product.css";
  include "$linkminisite" . "css/mpi.css";
  include "$linkminisite" . "css/normalize.css";
  include "$linkminisite" . "css/aside.css";
  ?>
</style>
</head>
<body>
    <? include "$linkminisite" . "inc/header-dinamic.php"; ?>
    <main class="main-tag-content">
        <div class="content" itemscope itemtype="https://schema.org/Article">
            <section>
                <?= $caminhoservicos ?>
                <div class="wrapper main-mpi-container">
                    <article class="description">
                        <div class="article-content">
                            <div class="ReadMore" style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                                <h2 class="h2-description">Descrição</h2>
                                <article class="p-description">
                                    <?= $conteudo5[0] ?>
                                </article>
                                <span id="readmore-open">Continuar Lendo...</span>
                                <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up" style="color: var(--azul-solucs);"></i></span>
                            </div>
                        </div>
                        <?
                        include "$linkminisite" . "inc/gallery.php";
                        ?>
                        <?
                        include "$linkminisite" . "inc/card-informativo.php";
                        // include("inc/especificacoes-mpi.php");
                        ?>
                    </article>
                    <?
                    include "$linkminisite" . "inc/coluna-lateral.php";
                    include "$linkminisite" . "inc/regioes.php";
                    include "$linkminisite" . "inc/aside-produtos.php";

                    include "$linkminisite" . "inc/copyright.php";
                    ?>
                </div><!-- .wrapper -->
                <div class="clear"></div>
            </section>
        </div>
    </main>
    <? include "$linkminisite" . "inc/footer.php"; ?>
    <script src="<?= $linkminisite ?>js/organictabs.jquery.js" ></script>

</body>

<script>
  document.addEventListener('DOMContentLoaded', function () {
    const pSideElements = document.querySelectorAll('.p-side');
    pSideElements.forEach(element => {
      element.addEventListener('click', function () {
        pSideElements.forEach(el => {
          el.classList.remove('p-side-active');
        });
        element.classList.add('p-side-active');
      });
    });
  });
</script>
<script>
  const btnCotar = document.querySelector('.btn-cotar');
  const modal = document.querySelector('.modal-btn');
  const closeBtn = document.querySelector('.modal-btn .close-btn');
  btnCotar.addEventListener('click', () => {
    modal.style.display = 'flex';
  });
  closeBtn.addEventListener('click', () => {
    modal.style.display = 'none';
  });
  window.addEventListener('click', (e) => {
    if (e.target === modal) {
      modal.style.display = 'none';
    }
  });
</script>
</html>
