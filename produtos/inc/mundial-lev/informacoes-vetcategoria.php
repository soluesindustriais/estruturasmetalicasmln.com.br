<?
$h1         = 'Informações';
$title      = 'Informações';
$desc       = 'Informações';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Informações';
include('inc/head.php');
?>
</head>

<body>
    <? include('inc/topo.php'); ?>

    <main>
        <div class="content">
            <section>
                <?= $caminho ?>
                <div class="wrapper">
                    <article class="full">
                        <h2>Conheça nossos Produtos</h2>
                        <ul class="thumbnails-main">

                            <?php

                           

                            foreach ($vetCategorias as $categoria) {
                                $categoriaNameSemAcento = strtolower(remove_acentos($categoria)); // Remove acentos e substitui espaços por hifens
                                $CategoriaNameUpper = ucwords(str_replace('-', ' ', $categoria)); // Substitui hifens por espaços e coloca a primeira letra de cada palavra em maiúscula

                                // Gerar um número aleatório entre 1 e 12
                                $numeroAleatorio = rand(1, 12);

                                // Estrutura HTML do elemento LI
                                echo "<li>
                                <div class=\"overflow-hidden\"><a rel=\"nofollow\" href=\" $url$categoriaNameSemAcento-categoria\" title=\"$CategoriaNameUpper\"><img src=\"$url" . "imagens/$categoriaNameSemAcento/$categoriaNameSemAcento-$numeroAleatorio.webp\" alt=\"$CategoriaNameUpper\" title=\"$CategoriaNameUpper\"></a></                 div>
                                <div class=\"title-produtos\">
                                <h2>$CategoriaNameUpper</h2>
                                </div>
                                </li>";
                            }
                            ?>

                        </ul>
                        <br class="clear">
                    </article>
                </div>
            </section>
        </div>
    </main>
    <? include('inc/footer.php'); ?>
</body>

</html>