<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Mundial Lev';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Sua necessidade é a nossa solução';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);




//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
"empresa-de-instalacao-de-tapumes",
"empresas-de-estruturas-metalicas",
"estrutura-metalica-para-cobertura",
"estrutura-metalica-para-galpao",
"fechamento-de-galpao-com-telha",
"veneziana-industrial-aluminio",
"veneziana-industrial-em-policarbonato",
"veneziana-industrial-metalica",
"veneziana-industrial-translucida",
"venezianas-industriais-em-pvc",
];

//Criar página de Serviço
/* $VetPalavrasInformacoes = [

]; */



// Numero do formulario de cotação
$formCotar = 200;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Mundial Lev';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Sua necessidade é a nossa solução';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Avenida, Rua José Marques Ribeiro, 1000';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Guaturinho';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Cajamar';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '07756-640';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'mundiallev@gmail.com';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #2d2d2f;
        --cor-secundariadocliente: #4c97bb;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>