<div class="columm-imagens">
    <? $imagens = glob("$linkminisite"."imagens/informacoes/" . $urlPagina . "-{,[0-3]}[0-3].webp", GLOB_BRACE);
    foreach ($imagens as $key => $imagem) : ?>
        <div class="img-box light-box-shadow">

            <img src="<?= $imagem ?>" alt="<?= $h1 ?>" title="<?= $h1 ?>">

        </div>
    <? endforeach; ?>
</div>
<div class="imagem-principal-anuncio">
    <a href="<?= $linkminisite . "imagens/informacoes/" . $urlPagina . "-1.webp" ?>" data-fancybox="group1" class="lightbox ancora-lightbox">
        <img src="<?= $linkminisite . "imagens/informacoes/" . $urlPagina . "-1.webp" ?>" alt="<?= $h1 ?>" title="<?= $h1 ?>">
    </a>
</div>