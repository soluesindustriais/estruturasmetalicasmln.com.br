<aside class="mpi-aside">
    <button class="botao-cotar botao-cotar-mobile btn-cotar" title="<?= $h1 ?>">
        Solicite um Orçamento
    </button>
    <? include "$linkminisite"."inc/btn-cotar.php"; ?>
    <div class="aside__info">
        <h2>INFORMAÇÕES</h2>
        <div class="informacoes-detalhes">
            <div class="info__container">
                <div class="info__content">
                    <i class="fa-solid fa-location-dot"></i>
                    <div class="info__text">
                        <p>Regiões de Atendimento</p>
                        <p><?= $cidade . ' - ' .  $bairro ?></p>
                    </div>
                </div>
                <div class="info__content">
                    <i class="fa-solid fa-industry"></i>
                    <div class="info__text">
                        <p>Especialidade</p>
                        <p><?= $segmento ?></p>
                    </div>
                </div>
                <div class="info__content">
                    <i class="fa-solid fa-phone-volume"></i>
                    <div class="info__text">
                        <p>Contato</p>
                        <p>(11) 98857-1030</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="aside__menu">
        <nav>
            <ul>
                <? include('inc/informacoes/informacoes-sub-menu.php'); ?>
            </ul>
        </nav>
    </div>
    <br>
</aside>
<div class="clear"></div>