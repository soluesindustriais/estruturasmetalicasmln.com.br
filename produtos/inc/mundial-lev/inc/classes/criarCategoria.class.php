
<?php

/*====================
|___Marcelo Serra____|
|  Data: 28/08/2020  |
====================*/


/*====================
|_____duda godoi_____|
|  Data: 03/01/2023  |
====================*/



class Categoria
{
    private $categorias;

    public function setCategorias($array)
    {
        $this->categorias = $array;
    }

    public function getCategorias()
    {
        return $this->categorias;
    }

    private function criarPaginaRaiz($file, $string)
    {
        $trata = new Trata();
        $stringUpperSemHifen = ucwords($trata->retiraHifen($string));
        $stringSemAcento = $trata->trataAcentos($string);
        $text =

            '
            <? $h1 = "' . $stringUpperSemHifen . ' - Categoria"; $title  = "' . $stringUpperSemHifen . ' - Categoria"; $desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo"; $key  = ""; include("inc/head.php"); include("inc/' . $stringSemAcento . '/' . $stringSemAcento . '-vetPalavras.php"); ?> <body> <? include("inc/topo.php"); ?><main role="main"> <div class="content"> <section> <?= $caminhocateg ?> <div class="wrapper-produtos"> <?php include_once("inc/' . $stringSemAcento . '/' . $stringSemAcento . '-buscas-relacionadas.php"); ?> <br class="clear"> <h1 style="text-align: center;"><?= $h1 ?></h1> <article class="full"> <div class="article-content"> <p>O mercado de <?= $h1 ?> é amplo e conta com produtos e serviços que podem ser úteis em diversas aplicações. No Soluções Industriais, portal especializado na geração de negócios para o mercado B2B, é possível encontrar as melhores empresas que atuam nesse segmento.</p> </div> <ul class="thumbnails-main"> <?php include_once("inc/' . $stringSemAcento . '/' . $stringSemAcento . '-categoria.php"); ?> </ul> </article> </section> </div> </main> </div><!-- .wrapper --> <? include("inc/footer.php"); ?> </body> </html>
            ';

        $this->writeText($file, $text);
    }

    private function writeText($file, $string)
    {
        $handle = fopen($file, "w");
        fwrite($handle, $string);
        fclose($handle);
    }
    private function criarPaginaCategoria($categoria)
    {
        $trata = new Trata();
        $categoriaFormatada = ucwords(str_replace('-', '_', $categoria)); // Substitui hífens por underlines e coloca a primeira letra de cada palavra em maiúsculo
        // Normaliza a string removendo acentos e substituindo "ç" por "c"
        $categoriaFormatada = iconv('UTF-8', 'ASCII//TRANSLIT', $categoriaFormatada);
        $categoriaFormatada = preg_replace('/[^A-Za-z0-9_ ]/', '', $categoriaFormatada); // Remove caracteres especiais remanescentes
        $categoriaFormatada = str_replace('ç', 'c', $categoriaFormatada);

        // Nome da variável dinâmico
        $nomeVarVetPalavras = 'VetPalavras' . str_replace(' ', '', $categoriaFormatada);
        $nomeVarCategoriaName = 'CategoriaName' . str_replace(' ', '', $categoriaFormatada);
        $categoriaTratada = $trata->trataAcentos($categoria);
        $stringSemAcento = $trata->trataAcentos($categoria);
        $stringUpperSemHifen = ucwords($trata->retiraHifen($categoria));
        $diretorio = "./inc/" . $categoriaTratada . "/";

        // Lista de sufixos de arquivo
        $sufixos = [
            'buscas-relacionadas.php',
            'coluna-lateral.php',
            'sub-menu-categoria.php',
            'sub-menu.php',
            'categoria.php',
            'galeria-fixa.php',
            'vetPalavras.php'
        ];

        // Criar cada arquivo na lista
        foreach ($sufixos as $sufixo) {
            $nomeArquivo = $diretorio . $categoriaTratada . "-" . $sufixo;
            $this->writeText($nomeArquivo, "");
        }


        $conteudoSubMenu = '<?php

        foreach ($' . $nomeVarVetPalavras . ' as $palavra) {
            $palavraSemAcento = strtolower(remove_acentos($palavra));
            $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $palavra));
        
            echo "<li><a class=\"link-sub-list first-child\" href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>\n";
        }
        
        ?>';


        // Criar arquivo sub-menu.php com conteúdo específico
        $nomeArquivoSubMenu = $diretorio . $categoriaTratada . "-sub-menu.php";
        $this->writeText($nomeArquivoSubMenu, $conteudoSubMenu);

        // Conteúdo específico para galeria-fixa.php
        $conteudoGaleriaFixa = '
 <link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
 <div class="galeria-fixa">
     <span class="galeria-ilustrativa">Galeria Ilustrativa</span>
     <ul class="thumbnails-mod17">
         <?php
 
         shuffle($' . $nomeVarVetPalavras . ' ); // Embaralha o array de palavras
 
         for ($i = 1; $i <= 12; $i++) {
             $categoriaNameSemAcento = strtolower(remove_acentos($' . $nomeVarCategoriaName . ')); // Remover acentos e substituir espaços por hifens
             $palavraAtual = $' . $nomeVarVetPalavras . ' [$i - 1]; // Obtém a palavra atual (ajustando o índice para começar de 0)
             $palavraSemAcento = strtolower(str_replace(" ", "-", remove_acentos($palavraAtual)));
 
             echo "<li><a class=\"lightbox\" href=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" title=\"" . $palavraAtual . "\"><img class=\"lazyload\" data-src=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" alt=\"" . $palavraAtual . "\" title=\"" . $palavraAtual . "\" /></a></li>";
         }
         ?>
     </ul>
 </div>';

        // Criar arquivo galeria-fixa.php com conteúdo específico
        $nomeArquivoGaleriaFixa = $diretorio . $categoriaTratada . "-galeria-fixa.php";
        $this->writeText($nomeArquivoGaleriaFixa, $conteudoGaleriaFixa);


        // Conteúdo específico para categoria.php
        $conteudoCategoria = '<?php

  $categoriaNameSemAcento = strtolower(remove_acentos($' . $nomeVarCategoriaName . ')); // Remover acentos e substituir espaços por hifens

  foreach ($' . $nomeVarVetPalavras . '  as $palavra) {
      $palavraSemAcento = strtolower(remove_acentos($palavra)); // Remover acentos e substituir espaços por hifens
      $palavraUpperCaseSemHifen = ucwords(str_replace("-", " ", $palavra)); // Substituir hifens por espaços e colocar em maiúsculas

      // Gerar número aleatório entre 1 e 12
      $numeroAleatorio = rand(1, 12);

      // Construir a estrutura LI
      echo "<li>
              <div class=\"overflow-hidden\"> <a rel=\"nofollow\" href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraUpperCaseSemHifen\"><img src=\"imagens/$categoriaNameSemAcento/$categoriaNameSemAcento-$numeroAleatorio.webp\" alt=\"$palavraUpperCaseSemHifen\" title=\"$palavraUpperCaseSemHifen\"></a> </div>
              <div class=\"title-produtos\">
                  <h2>$palavraUpperCaseSemHifen</h2>
              </div>
            </li>";
  }

  ?>';

        // Criar arquivo categoria.php com conteúdo específico
        $nomeArquivoCategoria = $diretorio . $categoriaTratada . "-categoria.php";
        $this->writeText($nomeArquivoCategoria, $conteudoCategoria);

        // Conteúdo específico para sub-menu-categoria.php
        $conteudoSubMenuCategoria = '<?php

        include \'inc/vetCategorias.php\';
        foreach ($vetCategorias as $categoria) {
      
        $nomeDaCategoriaSemAcentoComHifen = remove_acentos($categoria);
        
        // Substituindo hifens por espaços e depois aplicando ucwords
        $NomeDaCategoriaUpper = ucwords(str_replace(\'-\', \' \', $categoria));

        echo "<li><a href=\"" . $url . $nomeDaCategoriaSemAcentoComHifen . "-categoria\" title=\"Categoria - " . $NomeDaCategoriaUpper . "\">Categoria - " . $NomeDaCategoriaUpper . "</a></li>\n";
        }

        ?>';

        // Criar arquivo sub-menu-categoria.php com conteúdo específico
        $nomeArquivoSubMenuCategoria = $diretorio . $categoriaTratada . "-sub-menu-categoria.php";
        $this->writeText($nomeArquivoSubMenuCategoria, $conteudoSubMenuCategoria);


        // Conteúdo específico para vetPalavras.php
        $conteudoVetPalavras = "<?php\n\n" .
            "    \$$nomeVarCategoriaName = \"$categoriaTratada\";\n\n" .
            "    \$$nomeVarVetPalavras = [\n" .
            "        // Palavras relacionadas a \$$nomeVarVetPalavras\n" .
            "    ];\n" .
            "?>";

        // Criar arquivo vetPalavras.php com conteúdo específico
        $nomeArquivoVetPalavras = $diretorio . $categoriaTratada . "-vetPalavras.php";
        $this->writeText($nomeArquivoVetPalavras, $conteudoVetPalavras);



        // Conteúdo específico para buscas-relacionadas.php
        $conteudoBuscasRelacionadas = '
        <div class="busca-relacionadas">
            <b>Buscas relacionadas:</b>
            <ul class="relacionada">
                <?php
                $random = array();
                $limit = 3;
        
                foreach ($' . $nomeVarVetPalavras . '  as $pagina) {
                    $palavraSemAcento = strtolower(remove_acentos($pagina));
                    $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $pagina));
        
                    $random[] = "<li><a href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>";
                }
        
                shuffle($random);
                for ($i = 0; $i < min($limit, count($random)); $i++) {
                    echo $random[$i];
                }
                ?>
            </ul>
        </div>';

        // Criar arquivo buscas-relacionadas.php com conteúdo específico
        $nomeArquivoBuscasRelacionadas = $diretorio . $categoriaTratada . "-buscas-relacionadas.php";
        $this->writeText($nomeArquivoBuscasRelacionadas, $conteudoBuscasRelacionadas);



        // Conteúdo específico para imagens-fixos.php
        $conteudoImagensFixos = '
        <div class="grid">
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/' . $stringSemAcento . '/' . $stringSemAcento . '-01.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img class="lazyload" data-src="<?= $url ?>imagens/' . $stringSemAcento . '/' . $stringSemAcento . '-01.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/' . $stringSemAcento . '/' . $stringSemAcento . '-02.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img class="lazyload" data-src="<?= $url ?>imagens/' . $stringSemAcento . '/' . $stringSemAcento . '-02.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
        </div>';

        // Criar arquivo imagens-fixos.php com conteúdo específico
        $nomeArquivoImagensFixos = $diretorio . $categoriaTratada . "-imagens-fixos.php";
        $this->writeText($nomeArquivoImagensFixos, $conteudoImagensFixos);

        // Conteúdo específico para coluna-lateral.php
        $conteudoColunaLateral = '
        <aside>
            <h2><a href="<?= $url ?>' . $stringSemAcento . '-categoria" title="Produtos relacionados <?= $nomeSite ?>">' . $stringUpperSemHifen . '<br>Produtos relacionados</a></h2>
            <nav>
                <ul> <? include(\'inc/' . $stringSemAcento . '/' . $stringSemAcento . '-sub-menu.php\'); ?> </ul>
            </nav> <br>
        </aside>
        <aside>
            <h2><a href="<?= $url ?>produtos" title="Outras Categorias">Outras Categorias </a></h2>
            <nav class="no-scroll">
                <ul> <? include(\'inc/' . $stringSemAcento . '/' . $stringSemAcento . '-sub-menu-categoria.php\'); ?> </ul>
            </nav> <br>
        </aside>';

        // Criar arquivo coluna-lateral.php com conteúdo específico
        $nomeArquivoColunaLateral = $diretorio . $categoriaTratada . "-coluna-lateral.php";
        $this->writeText($nomeArquivoColunaLateral, $conteudoColunaLateral);


        $conteudoProdutosPremium = '
            <ul class="thumbnails">
                <? $random = array();
                $limit = 4;
                // aqui o produto
            
                shuffle($random);
                for ($i = 0; $i < $limit; $i++) {
                    print $random[$i];
                }
                ?>
            </ul>';

        // Criar arquivo produtos-premium.php com conteúdo específico
        $nomeArquivoProdutosPremium = $diretorio . $categoriaTratada . "-produtos-premium.php";
        $this->writeText($nomeArquivoProdutosPremium, $conteudoProdutosPremium);






        $conteudoProdutosRandom = '
            <?
            $random = array();
            $limit = 7;
            // aqui o produto
            
            
            shuffle($random);
            
            for ($i = 0; $i < $limit; $i++) {
                print $random[$i];
            } ?>
            ';

        // Criar arquivo produtos-random.php com conteúdo específico
        $nomeArquivoProdutosRandom = $diretorio . $categoriaTratada . "-produtos-random.php";
        $this->writeText($nomeArquivoProdutosRandom, $conteudoProdutosRandom);
    }

    private function criarDiretorio($categoria)
    {
        $trata = new Trata();
        $categoriaTratada = $trata->trataAcentos($categoria);

        $diretorio = "./inc/" . $categoriaTratada;

        if (!is_dir($diretorio)) {
            mkdir($diretorio, 0777, true);
            $this->criarPaginaCategoria($categoria);
        }
    }

    public function criarCategoria()
    {
        foreach ($this->categorias as $categoria) {
            $this->checkFileExists($categoria);
            $this->criarDiretorio($categoria);
        }
    }

    private function checkFileExists($string)
    {
        $trata = new Trata();
        $stringSemAcentos = $trata->trataAcentos($string);
        $file = $stringSemAcentos . "-categoria.php";
        if (!file_exists("./" . $file)) {
            $this->criarPaginaRaiz($file, $string);
        }
    }

    private function checkDirExists($string)
    {
        $trata = new Trata();
        $stringSemAcentos = $trata->trataAcentos($string);
        $file = $stringSemAcentos;
        if (!is_dir("./inc/" . $file)) {
            mkdir("./inc/" . $file, 0777);
        }
    }
};

?>

