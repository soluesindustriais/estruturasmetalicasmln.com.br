<section class="especificacoes">
    <h2 class="color-blue">ESPECIFICAÇÕES <span class="color-green">TÉCNICAS</span></h2>

    <div class="table-mpi flex-wrap-mobile-phone">
        <div class="table-info">
            <h3>Características <span class="color-green">Gerais</span></h3>
            <div class="table-content">
                <table>
                    <tbody>
                        <tr>
                            <td>Modelo</td>
                            <td>Aço Galvanizado</td>
                        </tr>
                        <tr>
                            <td>Marca</td>
                            <td>[Nome da Marca]</td>
                        </tr>
                        <tr>
                            <td>Largura</td>
                            <td>130mm</td>
                        </tr>
                        <tr>
                            <td>Comprimento</td>
                            <td>200m</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="table-info">
            <h3>Especificações <span class="color-green">Técnicas</span></h3>
            <div class="table-content">
                <table>
                    <tbody>
                        <tr>
                            <td>Material</td>
                            <td>Aço Galvanizado</td>
                        </tr>
                        <tr>
                            <td>Diâmetro</td>
                            <td>130mm</td>
                        </tr>
                        <tr>
                            <td>Comprimento</td>
                            <td>200m</td>
                        </tr>
                        <tr>
                            <td>Tipo</td>
                            <td>Tubo</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>