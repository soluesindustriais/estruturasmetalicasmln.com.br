<section class="wrapper">
	<div class="section-text">
		<h4 class="sub-title">Blog & Artigos</h4>
		<h2 class="text-center title">Últimos Artigos</h2>
		<div class="d-flex content-feature flex-wrap-mobile">
			<div class="card-feature">
				<div class="img-card-feature">
					<img src="imagens/banner/banner-midias.jpg" alt="imagens-1">
				</div>
				<div class="content-card-feature">
					<ul class="blog-info">
						<li><i class="fa-solid fa-user"></i> Author</li>
						<li><i class="fa-regular fa-calendar"></i> 09 Jan 2003</li>
					</ul>
					<h3>Alcance Amplo com Divulgação em Mais de 60 Canais Sociais</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio totam voluptas error </p>
					<span class="readmore"><i class="fa-solid fa-arrow-right"></i>Leia Mais</span>
				</div>
			</div>
			<div class="card-feature">
				<div class="img-card-feature">
					<img src="imagens/banner/banner-seo.jpg" alt="imagens-1">
				</div>
				<div class="content-card-feature">
					<ul class="blog-info">
						<li><i class="fa-solid fa-user"></i> Author</li>
						<li><i class="fa-regular fa-calendar"></i> 09 Jan 2003</li>
					</ul>
					<h3>Domine os Buscadores com Posicionamento Estratégico</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio totam voluptas error </p>
					<span class="readmore"><i class="fa-solid fa-arrow-right"></i>Leia Mais</span>
				</div>
			</div>
			<div class="card-feature">
				<div class="img-card-feature">
					<img src="imagens/banner/banner-marketing.jpg" alt="imagens-1">
				</div>
				<div class="content-card-feature">
					<ul class="blog-info">
						<li><i class="fa-solid fa-user"></i> Author</li>
						<li><i class="fa-regular fa-calendar"></i> 09 Jan 2003</li>
					</ul>
					<h3>Marketing Eficaz com Relatórios de Performance Exclusivos</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio totam voluptas error </p>
					<span class="readmore"><i class="fa-solid fa-arrow-right"></i>Leia Mais</span>
				</div>
			</div>
		</div>
	</div>
</section>