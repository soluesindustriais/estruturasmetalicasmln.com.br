<section class="wrapper flex-wrap-mobile section-content">
	<div class="section-text">
		<h4 class="sub-title">O que nós fornecemos</h4>
		<h2 class="text-center title">Nossos Serviços</h2>

		<ul class="list-benefits d-flex flex-wrap-mobile justify-content-between">
			<li>
				<div class="icon-services"><i class="fa-solid fa-certificate"></i></div>
				<h3 class="sub-title">Qualidade e Preço Justo</h3>
				<p>Há anos atuando no mercado, a LUCI COMERCIO se destaca pelo compromisso em oferecer produtos de alta qualidade e preços justos. Nosso compromisso vai além de simples transações comerciais, buscando constantemente superar as expectativas de nossos clientes. <strong>Experimente a qualidade LUCI COMERCIO!</strong></p>
			</li>

			<li>
				<div class="icon-services"><i class="fa-solid fa-handshake"></i></div>
				<h3 class="sub-title">Atendimento Excepcional</h3>
				<p>Nossa equipe dedicada trabalha incansavelmente para atender às necessidades específicas de cada cliente. Com um amplo portfólio de produtos, oferecemos soluções que se adequam perfeitamente aos seus requisitos. <strong>Conte conosco para um atendimento de excelência!</strong></p>
			</li>

			<li>
				<div class="icon-services"><i class="fa-solid fa-clock"></i></div>
				<h3 class="sub-title">Pontualidade e Eficiência</h3>
				<p>Valorizamos a pontualidade e cumprimos rigorosamente os prazos de distribuição acordados, garantindo que você receba seus pedidos no momento certo. Nosso compromisso com a excelência reflete-se no nosso atendimento ao cliente, sempre pronto para ajudar. <strong>Confie na LUCI COMERCIO para uma entrega pontual!</strong></p>
			</li>
		</ul>


	</div>
</section>