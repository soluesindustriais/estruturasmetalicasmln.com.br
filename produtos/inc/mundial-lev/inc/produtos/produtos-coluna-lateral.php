
        <aside>
            <h2><a href="<?= $url ?>produtos-categoria" title="Produtos relacionados <?= $nomeSite ?>">Produtos<br>Produtos relacionados</a></h2>
            <nav>
                <ul> <? include('inc/produtos/produtos-sub-menu.php'); ?> </ul>
            </nav> <br>
        </aside>
        <aside>
            <h2><a href="<?= $url ?>produtos" title="Outras Categorias">Outras Categorias </a></h2>
            <nav class="no-scroll">
                <ul> <? include('inc/produtos/produtos-sub-menu-categoria.php'); ?> </ul>
            </nav> <br>
        </aside>