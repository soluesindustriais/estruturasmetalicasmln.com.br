<?php

    /**
     * <b>Dados da Doutores da Web</b>
     * Configurações dos dados da Doutores da Web
     */
    define('AGENCY_CONTACT', 'MPI Technology'); //Nome do contato
    define('AGENCY_EMAIL', 'doutores@doutoresdaweb.com.br'); //E-mail de contato
    define('AGENCY_PHONE', '(11) 3044-6284'); //Telefone de contato
    define('AGENCY_URL', 'http://www.doutoresdaweb.com.br'); //URL completa do seu site/portfolio
    define('AGENCY_NAME', 'MPI Technology'); //Nome da sua agência web
    define('AGENCY_ADDR', 'Av. Guido Caloi, 1985'); //Endereço da sua agência web (RUA, NÚMERO)
    define('AGENCY_CITY', 'São Paulo');  //Endereço da sua agência web (CIDADE)
    define('AGENCY_UF', 'SP');  //Endereço da sua agência web (UF DO ESTADO)
    define('AGENCY_ZIP', '00000-000');  //Endereço da sua agência web (CEP)
    define('AGENCY_COUNTRY', 'Brasil');  //Endereço da sua agência web (PAÍS)


