<?php

/**
 * <b>Importador de arquivos</b>
 * ImportFiles.class.php [CLASS]
 * Classe responsável por importar e modela os arquivos para importar no banco de dados
 * @copyright (c) 2017, Rafael da Silva Lima Doutores da Web
 */
class ImportFiles {

  //Armazena todas as informações do arquivo importado
  Private $File;
  Private $Data;
  //Define os tipo de arquivos permitidos na importação
  Private $Types;
  //Saída de dados
  Private $Result;
  Private $Error;
  Private $Configs;

  /**
   * <b>Inicializador do Import</b>
   * @param array $File = Arquivo do tipo CSV
   */
  function __construct($File) {
    $this->File = $File;

    //Tipos de arquivos permitos MIME TYPES
    $this->Types = array(
      "application/octet-stream",
      "application/vnd.ms-excel",
      "application/x-csv",
      "application/csv"
    );

    //Verifica os dados e prossegue com a importação
    $this->CheckData();
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  /**
   * <b>Retorno de dados</b>
   * Retorna um array com os dados tratados para importação, deve ser utilizado em outro método.
   * @return array = Dados envelopados
   */
  public function getData() {
    return $this->Data;
  }

  /**
   * <b>Retorno de dados</b>
   * Retorna um array com as configs do arquivo
   * @return array = Dados envelopados
   */
  public function getConfigs() {
    return $this->Configs;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################

  private function CheckData() {
    if (!in_array($this->File['type'], $this->Types)):
      $this->Error = array("Este formato de arquivo não é válido.", WS_ERROR, "Aviso!");
      $this->Result = false;
    else:
      $this->CheckFile();
    endif;
  }

  private function CheckFile() {
    $this->File['resource'] = fopen($this->File['tmp_name'], "r");

    if (!isset($this->File['resource'])):
      $this->Error = array("Não foi possível abrir o arquivo", WS_ERROR, "Aviso!");
      $this->Result = false;
    else:
      while ($data = fgetcsv($this->File['resource'], 0, ",")):
        $this->File['csv'][] = array_map('trim', $data);
      endwhile;
      if (count($this->File['csv']) == 0):
        $this->Error = array("Não foi possível carregar as linhas do arquivo ou ele está vazio", WS_ERROR, "Aviso!");
        $this->Result = false;
      else:
        $this->StructFile();
      endif;
      fclose($this->File['resource']);
    endif;
  }

  private function StructFile() {
    $Read = new Read;
    $Read->FullRead("SHOW COLUMNS FROM " . constant($this->File['table']));
    if (!$Read->getResult()):
      $this->Error = array("Não foi possível carregar os colunas do banco", WS_ERROR, "Aviso!");
      $this->Result = false;
    else:
      foreach ($Read->getResult() as $key => $value):
        $this->File['Fields'][] = $value['Field'];
      endforeach;
      $this->SetFields();
    endif;
  }

  private function SetFields() {
    //Remove os 3 primeiros índices do array de Fields do banco
    for ($u = 0; $u <= 3; $u++):
      array_shift($this->File['Fields']);
    endfor;

    $c = 0; //conta as linhas do csv
    foreach ($this->File['csv'] as $registros => $valores):
      if (count($this->File['Fields']) != count($valores)):
        $this->Result = false;
        $this->Error = array("O número de campos do arquivo não corresponde ao layout do banco de dados.", WS_ERROR, "Aviso!");
        break;
      endif;
      $r = 0; //conta o campos do banco
      foreach ($this->File['Fields'] as $id => $name):
        $this->Data[$c][$name] = $valores[$r];
        $r++;
      endforeach;
      $c++;
    endforeach;

    if (count($this->Data) == count($this->File['csv'])):      
      unset($this->File['csv']);      
      $this->Result = true;
      $this->Configs['name'] = $this->File['name'];
      $this->Configs['cat_parent'] = $this->File['cat_parent'];
      $this->Configs['table'] = $this->File['table'];
      $this->Configs['baseDir'] = $this->File['baseDir'];      
      $this->Error = array("Seu arquivo passou pela importação, confira as informações abaixo para continuar.", WS_ACCEPT, "Tudo certo!");
    endif;
  }

}
