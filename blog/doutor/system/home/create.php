<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <?php
  $ReadRecursos = new Read;
  $ReadRecursos->ExeRead(TB_HOME, "WHERE user_empresa = :emp", "emp={$_SESSION['userlogin']['user_empresa']}");
  if ($ReadRecursos->getRowCount() >= 1):
    ?>
    <div class="x_panel">

      <div class="x_content">
        <div class="x_title">
          <h2>Você só pode criar uma página inicial!</h2>                           
          <div class="clearfix"></div>                            
        </div>

        <div class="x_content">
          <p>Para editar sua Página inicial criada, navegue até o menu <b>Página inicial ></b> <a href="painel.php?exe=home/update" class="btn btn-primary btn-xs">Editar</a></p>                
        </div>
      </div>
    </div>
  <?php else: ?>
    <form data-parsley-validate class="form-horizontal form-label-left" method="post">
      <div class="page-title">
        <div class="title_left">
          <h3><i class="fa fa-home"></i> Cadastro da página inicial (Home)</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12"> 
            <div class="pull-right">
              <button type="submit" name="Cadastrar" class="btn btn-primary"><i class="fa fa-save"></i></button>
              <!--<button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=home/index'"><i class="fa fa-home"></i></button>-->
            </div>
            <div class="clearfix"></div>
            <br/>
            <?php
            $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if (isset($post) && isset($post['Cadastrar'])):
              unset($post['Cadastrar']);
              $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
              $attrs = null;
              if (!isset($post['attr_id'])):
                $post['attr_id'] = null;
              else:
                foreach ($post['attr_id'] as $attr_id):
                  $attrs .= $attr_id . ',';
                endforeach;
                $post['attr_id'] = trim($attrs, ',');
              endif;

              $cadastra = new HomePage();
              $cadastra->ExeCreate($post);

              var_dump($cadastra);

              if (!$cadastra->getResult()):
                $erro = $cadastra->getError();
                WSErro($erro[0], $erro[1], null, $erro[2]);
              else:
                $_SESSION['Error'] = $cadastra->getError();
                header('Location: painel.php?exe=home/create&get=true');
              endif;
            endif;

            if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
              //COLOCAR ALERTA PERSONALIZADOS
              WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
              unset($_SESSION['Error']);
            endif;
            ?>
            <div class="x_panel">
              <div class="x_content">
                <div class="x_title">
                  <h2>Preencha os dados abaixo para criar sua Página inicial.</h2>                           
                  <div class="clearfix"></div>                            
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_status">Ativar Página?</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="radio">
                      <label>
                        <input name="home_status" id="optionsRadios1" type="radio" value="2" <?php
                        if (isset($post['home_status']) && $post['home_status'] == 2): echo 'checked="true"';
                        endif;
                        ?>> Sim
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input name="home_status" id="optionsRadios2" type="radio" value="1" <?php
                        if (isset($post['home_status']) && $post['home_status'] == 1): echo 'checked="true"';
                        endif;
                        ?>> Não
                      </label>
                    </div>                 
                  </div>                                 
                </div> 
                <div class="clearfix"></div>      
                <hr>               

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_title">Título principal <span class="required">*</span>
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="home_title" name="home_title" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o título com a chamada principal da página" value="<?php
                    if (isset($post['home_title'])): echo $post['home_title'];
                    endif;
                    ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_description">Descrição, até 160 caractéres <span class="required">*</span>
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea name="home_description" 
                              required="required" 
                              class="form-control col-md-7 col-xs-12 j_contChars" 
                              data-parsley-trigger="keyup" 
                              data-parsley-minlength="140" 
                              data-parsley-maxlength="160" 
                              data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                              data-parsley-validation-threshold="139"><?php
                                if (isset($post['home_description'])): echo $post['home_description'];
                                endif;
                                ?></textarea>
                    <div class="j_cont"></div> 
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_keywords">Keywords</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">                                                                
                    <input id="tags_1" type="text" name="home_keywords" class="tags form-control col-md-7 col-xs-12" value="<?php
                    if (isset($post['home_keywords'])): echo $post['home_keywords'];
                    endif;
                    ?>">
                    <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                  </div>
                </div>

                <div class="form-group">                                
                  <label class="control-group col-md-12 col-sm-12 col-xs-12" for="home_content">Conteúdo da página (Uso Avançado)</label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <textarea name="home_content" class="form-control col-md-7 col-xs-12 j_word_full" rows="50"><?php
                      if (isset($post['home_content'])): echo $post['home_content'];
                      endif;
                      ?></textarea>
                  </div>
                </div>
                <hr>
<!--                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <h2>Atributos relacionados, selecione os widget para serem adicionado na página do site.</h2>                  
                  <?php foreach (Atributos() as $key => $value): ?>
                    <div class="checkbox col-md-4 col-sm-6 col-xs-12">
                      <label for="<?= $key; ?>">
                        <input type="checkbox" class="flat" id="<?= $key; ?>" name="attr_id[]" value="<?= $key; ?>"
                        <?php
                        if (isset($post['attr_id'])):
                          $attr_ids = explode(',', $post['attr_id']);
                          foreach ($attr_ids as $keys => $values):
                            if ($key == $values):
                              echo 'checked="true"';
                            endif;
                          endforeach;
                        endif;
                        ?>> <?= $value; ?>
                      </label>
                    </div>
                  <?php endforeach; ?>
                </div>-->

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="pull-right">
        <button type="submit" name="Cadastrar" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <!--<button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=home/index'"><i class="fa fa-home"></i></button>-->
      </div>
      <div class="clearfix"></div>
      <br/>
    </form>
  <?php endif; ?>
  <div class="clearfix"></div>
</div>
