$(document).ready(function() {

    $("#app_gallery").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-image'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Enviar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: true, // exibe as miniaturas        
            maxFileCount: 30,
            allowedFileExtensions: ["jpg", "gif", "png", "webp"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        }),
        $("#app_gallery_full").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-image'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Enviar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: true, // exibe as miniaturas        
            maxFileCount: 30,
            allowedFileExtensions: ["jpg", "gif", "png", "webp"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        }),
        $("#app_cover").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-image'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Enviar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: true, // exibe as miniaturas        
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "gif", "png", "webp"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        }), $(".app_post").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-image'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Enviar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: true, // exibe as miniaturas        
            maxFileCount: 1,
            allowedFileExtensions: ["jpg", "gif", "png", "webp"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group j_fechar {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        }),
        $("#app_file").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-file'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Enviar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: !1, // exibe as miniaturas        
            maxFileCount: 1,
            allowedFileExtensions: ["pdf", "csv", "doc", "xlsx", "docx"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        }),
        $("#app_file_csv").fileinput({
            browseClass: "btn btn-success",
            browseLabel: "Procurar",
            browseIcon: "<i class='fa fa-database'></i>&nbsp;",
            removeClass: "btn btn-danger",
            removeLabel: "Remover",
            removeIcon: "<i class='ti-trash'></i>&nbsp;",
            uploadClass: "btn btn-info",
            uploadLabel: "Carregar",
            uploadIcon: "<i class='ti-upload'></i>&nbsp;",
            showPreview: !1, // exibe as miniaturas        
            maxFileCount: 1,
            allowedFileExtensions: ["csv"], // extensões aceitas
            showUpload: !1, // exibe o botão de upload
            layoutTemplates: {
                overwriteInitial: !1,
                main1: "{preview}\n<div class='input-group {class}'>\n   <div class='input-group-btn'>\n       {browse}\n       {upload}\n       {remove}\n   </div>\n   {caption}\n</div>"
            }
        })
});