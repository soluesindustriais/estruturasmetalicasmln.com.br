<?
$h1         = 'Sobre nós';
$title      = 'Sobre nós';
$desc       = 'O canal de cotações Estruturas Metálicas MLN atende o mercado de estruturas metálicas, facilitando a busca do comprador, encontrando o fornecedor mais adequado';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Sobre nós';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
    <main>
        <div class="content">
            <section>
                <?=$caminho?>
                <h1><?=$h1?></h1>
                <p>O canal de cotações Estruturas Metálicas MLN foi criado com o objetivo de atender o mercado de estruturas metálicas, facilitando a busca do comprador encontrando o fornecedor que mais se enquadra em sua necessidade.</p>
                <p>A Estruturas Metálicas MLN faz parte do Soluções Industriais, uma plataforma B2B focada em geração de novos negócios, facilitando o contato entre as indústrias e os seus clientes em potencial.</p>
            <div class="sobre-nos-texto">
                <article class="full">
                    <h2>DIVULGUE NO SOLUÇÕES INDUSTRIAIS</h2>
                    <div class="sobre-nos-videos">
                        <video class="video-mpi" width="560" height="315" controls="controls"><source src="<?=$url?>imagens/solucoes-industriais-video-introducao.mp4" type="video/mp4"></video><br></div>
                        <p>Com técnicas de marketing digital aliado ao seu negócio, você estará à frente da sua concorrência, agregando valor a sua marca e ficando mais próximo de seus clientes e potenciais clientes, além de garantir um crescente aumento em seu faturamento.</p>
                        <p>Alavanque já suas vendas através do Soluções Industriais, a ferramenta mais completa para o seu negócio. Sua empresa foca em vender e o Soluções Industriais em gerar novas oportunidades!</p>
                </article>
            </div>
            <div>
                <h2 class="blue"><a href="https://automacao.marketingparaindustria.com.br/faca-parte-portal" target="blank">Faça parte da maior plataforma Industrial</a></h2>
                <br class="clear">
            </div>
                <br class="clear">
                <br class="clear">
            </section>
        </div>
    </main>
</div>
<? include('inc/footer.php');?>
</body>
</html>
