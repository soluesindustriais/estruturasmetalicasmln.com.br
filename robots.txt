User-agent: *

Disallow: /imagens/portal/thumbs/
Disallow: /imagens/cobertura-metalica/thumbs/
Disallow: /imagens/coberturas/thumbs/
Disallow: /imagens/estruturas-metalicas/thumbs/
Disallow: /imagens/lona-para-cobertura/thumbs/


Sitemap: https://www.estruturasmetalicasmln.com.br/sitemap.xml
